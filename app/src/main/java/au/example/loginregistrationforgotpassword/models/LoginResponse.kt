package au.example.loginregistrationforgotpassword.models

data class LoginResponse(val result: Boolean, val message:String, val user: User)