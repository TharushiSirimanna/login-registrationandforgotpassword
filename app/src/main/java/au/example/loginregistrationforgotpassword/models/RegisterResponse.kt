package au.example.loginregistrationforgotpassword.models

data class RegisterResponse (val result: Boolean, val message:String, val user: User )