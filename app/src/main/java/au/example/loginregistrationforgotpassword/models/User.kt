package au.example.loginregistrationforgotpassword.models

data class User(val email: String?, val password: String?, val password_confirmation: String?, val first_name: String?, val last_name:String?, val phone:String?, val timezone:String?)