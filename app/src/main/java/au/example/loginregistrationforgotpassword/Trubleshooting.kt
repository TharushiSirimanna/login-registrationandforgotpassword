package au.com.bse.presentation.util

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.util.Base64
import android.util.Log
import androidx.annotation.RequiresApi
import java.io.ByteArrayInputStream
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.security.cert.CertificateException
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate


object Trubleshooting {
    @RequiresApi(Build.VERSION_CODES.P)
    fun generateHash(context: Context): String {
        var encodeToString = ""
        var hexString = ""

        val info =
            context.packageManager.getPackageInfo(
                "tharushi.com.fbloginwithkotlin",
                PackageManager.GET_SIGNING_CERTIFICATES
            )
        val signature = info.signingInfo.apkContentsSigners[0]

        try {
            val md = MessageDigest.getInstance("SHA")
            md.update(signature.toByteArray())
            encodeToString = Base64.encodeToString(md.digest(), Base64.DEFAULT)
            Log.e("KeyHash:", encodeToString)
        } catch (e: PackageManager.NameNotFoundException) {

        } catch (e: NoSuchAlgorithmException) {

        }

        val input = ByteArrayInputStream(signature.toByteArray())

        var cf: CertificateFactory? = null

        try {
            cf = CertificateFactory.getInstance("X.509")


        } catch (e: CertificateException) {
            e.printStackTrace()
        }
        var c: X509Certificate? = null
        try {
            c = cf?.generateCertificate(input) as X509Certificate?
        } catch (e: CertificateException) {
            e.printStackTrace()
        }

        try {
            val md = MessageDigest.getInstance("SHA-1")
            val publicKey = md.digest(c?.publicKey?.encoded)

            hexString = byte2HexFormatted(publicKey)

            Log.e("Example", "Cer: $hexString")

        } catch (e1: NoSuchAlgorithmException) {
            e1.printStackTrace()
        }

        return encodeToString
    }

    private fun byte2HexFormatted(arr: ByteArray): String {

        val hexString = StringBuffer()
        for (i in arr) {
            var appendString = Integer.toHexString(i.toInt() and (0xFF))
            if (appendString.length == 1) hexString.append("0")
            hexString.append(appendString)
        }

        return hexString.toString()
    }
}