package au.example.loginregistrationforgotpassword.api
import au.example.loginregistrationforgotpassword.models.RegisterResponse
import au.example.loginregistrationforgotpassword.models.LoginResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*


val BASE_URL = "http://oxygen.sandbox8.elegant-media.com/api/v1/"

public interface Api {

    @FormUrlEncoded
    @Headers(
        "Content-Type: application/json",
        "x-api-key:suYSjwJx9xLHdMcUSN9jIBnEtBZKJUEPturs6aJ1hc748G966bOL"
    )
    @POST("register")

    fun registerUser(

        @Field("email") email: String,
        @Field("password") password: String,
        @Field("password_confirmation") password_confirmation:String,
        @Field("first_name") first_name: String,
        @Field("last_name") last_name: String,
        @Field("phone") phone: String,
        @Field("timezone") timezone: String,
        @Field("device_id") device_id:String,
        @Field("device_type") device_type:String,
        @Field("device_push_token") device_push_token: String

    ): Call<RegisterResponse>

    @FormUrlEncoded
    @Headers(
        "Content-Type: application/json",
        "API KEY:suYSjwJx9xLHdMcUSN9jIBnEtBZKJUEPturs6aJ1hc748G966bOL"
    )
    @POST("login")

    fun loginUser(

        @Field("email")id: String,
        @Field("password")password:String,
        @Field("device_id")device_id: String,
        @Field("device_type")device_type:String,
        @Field("device_push_token")device_push_token:String

    ): Call<LoginResponse>


    object RetrofitClient {

        public fun newApiService(): Api {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create(Api::class.java)
        }
    }
}

