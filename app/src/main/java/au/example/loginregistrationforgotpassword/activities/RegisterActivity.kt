package au.example.loginregistrationforgotpassword.activities


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.telecom.Call
import android.widget.Toast
import au.example.loginregistrationforgotpassword.R
import au.example.loginregistrationforgotpassword.api.Api
import au.example.loginregistrationforgotpassword.models.LoginResponse
import au.example.loginregistrationforgotpassword.models.RegisterResponse
import au.example.loginregistrationforgotpassword.storage.SharedPrefManager
import kotlinx.android.synthetic.main.activity_register.*
import retrofit2.Response
import javax.security.auth.callback.Callback


class RegisterActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        textViewLogin.setOnClickListener {
            startActivity(Intent(this@RegisterActivity, LoginActivity::class.java))
        }


        buttonSignUp.setOnClickListener {
            val email = editTextEmail.text.toString().trim()
            val password = editTextPassword.text.toString().trim()
            val password_confirmation = editTextConfirmPassword.text.toString().trim()
            val first_name = editTextFirstName.text.toString().trim()
            val last_name = editTextLastName.text.toString().trim()
            val phone = editTextPhone.text.toString().trim()
            val timezone = editTextTimezone.text.toString().trim()


            if (email.isEmpty()) {
                editTextEmail.error = "Email required"
                editTextEmail.requestFocus()
                return@setOnClickListener
            }

            if (password.isEmpty()) {
                editTextPassword.error = "Password required"
                editTextPassword.requestFocus()
                return@setOnClickListener
            }

            if (password_confirmation.isEmpty()) {
                editTextConfirmPassword.error = "Confirm Password required"
                editTextConfirmPassword.requestFocus()
                return@setOnClickListener
            }

            if (first_name.isEmpty()) {
                editTextFirstName.error = "First Name required"
                editTextFirstName.requestFocus()
                return@setOnClickListener
            }


            if (last_name.isEmpty()) {
                editTextLastName.error = "Last Name required"
                editTextLastName.requestFocus()
                return@setOnClickListener
            }

            if (phone.isEmpty()) {
                editTextPhone.error = "Phone required"
                editTextPhone.requestFocus()
                return@setOnClickListener
            }

            if (timezone.isEmpty()) {
                editTextTimezone.error = "Timezone required"
                editTextTimezone.requestFocus()
                return@setOnClickListener
            }


            Api.RetrofitClient.newApiService().registerUser(
                email,
                password,
                password_confirmation,
                first_name,
                last_name,
                phone,
                "GMT+5.30",
                "1234567",
                "Android",
                ""
            )
                .enqueue(object : retrofit2.Callback<RegisterResponse> {
                    override fun onFailure(call: retrofit2.Call<RegisterResponse>, t: Throwable) {
                        Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()

                    }

                    override fun onResponse(
                        call: retrofit2.Call<RegisterResponse>,
                        response: Response<RegisterResponse>
                    ) {
                        if (!response.body()?.result!!) {

                            SharedPrefManager.getInstance(applicationContext)
                                .saveUser(response.body()?.user!!)

                            val intent = Intent(applicationContext, ProfileActivity::class.java)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

                        } else {

                            Toast.makeText(
                                applicationContext,
                                response.body()?.message,
                                Toast.LENGTH_LONG
                            ).show()

                        }
                    }

                })
        }

    }
}
